Mysterious Card Online
=======

Description
----------


A program that allow people to chat with each other. It also provide card game to play.


Features
-----------


Chat with other people.


Roll the dice and see how much do you get.


Can create private room to chat one on one.


Mysterious card game that you will never know what is under the card.

