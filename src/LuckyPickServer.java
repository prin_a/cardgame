import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;

/**
 * Server for lucky pick game type.
 * @author Prin Angkunanuwat
 *
 */
public class LuckyPickServer extends AbstractServer implements GameServer{

	private String roomID;
	private String hostName;
	private String gameType;
	private ConnectionToClient player1;
	private ConnectionToClient player2;
	private ChatServer mainsv;
	private int numplayer;

	/**
	 * Construct server with hostname and port.
	 * @param port used by server.
	 * @param id of the room.
	 * @param host is a name of user who create the room.
	 * @param mainsv refer to main server.
	 */
	public LuckyPickServer(int port,int id,String host,ChatServer mainsv) {
		super(port);
		hostName = host;
		roomID = "room" + id;
		gameType = "LuckyPick";
		numplayer = 0;
		this.mainsv = mainsv;
	}
	/**
	 * Recieve message from client and decide what to do.
	 * Take first eleven charractor as a header.
	 */
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		if ( ! ( msg instanceof String )) {
			sendToClient(client,"s send msg Unrecognized Message");
			return;
		}

		String message = (String) msg;
		String header = message.substring(0, 11);

		switch(header){
		case "sendmessage":
			sendToAllClients( "sendmessage" + client.getInfo("username") + ": " + message.substring(11).trim());
			break;
		case "getroominfo":
			sendToClient(client,"retroominfo" + getRoomInfo());
			break;
		case "player2name":
			client.setInfo("username", message.substring(11).trim());
			sendToAllClients("player2name" + message.substring(11).trim());
			break;
			
		}
	}
	/**
	 * When client connect the number of player will be changed.
	 */
	protected void clientConnected(ConnectionToClient client) {
		if( numplayer == 0 ){
			player1 = client;
			client.setInfo("username", hostName);
		} else {
			player2 = client;
		}
		numplayer++;
		numPlayerChange(numplayer);
	}
	/**
	 * When client disconnect the number of player will be changed.
	 * If the host disconnect, the server will close itself.
	 */
	synchronized protected void clientDisconnected(ConnectionToClient client) {
		if(player1 == client){
			mainsv.removeServer(this);
			this.stopListening();
			try {
				this.close();
			} catch (IOException e) {
			}
		} else if (player2 == client){
			player2 = null;
			numplayer--;
			numPlayerChange(numplayer);
		}
	}
	/**
	 * Send message to specific client.
	 * @param client to receive message.
	 * @param message to be send.
	 */
	protected void sendToClient(ConnectionToClient client, String message){
		try {
			client.sendToClient(message);
		} catch (IOException e) {
		}
	}
	/**
	 * Get room id of server.
	 */
	@Override
	public String getRoomID() {
		return roomID;
	}
	/**
	 * Get number of player.
	 */
	public int getNumplayer(){
		return numplayer;
	}
	/**
	 * Get all server info.
	 */
	@Override
	public String getRoomInfo() {
		return roomID + " " + hostName + " " + gameType + " " + numplayer ;
	}
	/**
	 * Tell the client that the number of player is changed.
	 * @param numplayer is current number of client in server.
	 */
	public void numPlayerChange(int numplayer) {
		sendToClient(player1, "numchanges " + numplayer);
	}

}
