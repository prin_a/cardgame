import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

/**
 * UI for luckypick game.
 * @author Prin Angkunanuwat
 *
 */
public class LuckyPickClientUI extends JFrame{
	private JTextArea chatAreaP;
	private JTextArea typeAreaP;
	private JCheckBox moveToBottom;

	private LuckyPickClient client;
	private ImageIcon[] icon;
	private ImageIcon backIcon;
	private JButton[] cardButton;
	private JTextField player2name;
	private JTextField player1name;
	private JTextField infoRoomID;
	private JTextField infoHostName;
	private JTextField infoGameType;
	private JTextField infoNumplayer;
	private String[] message;
	/**
	 * Construct and set the refer client.
	 * @param client
	 */
	public LuckyPickClientUI(LuckyPickClient client){
		super("Chat");
		this.client = client;
		this.client.setUI(this);
		this.setSize(800, 600);
		this.initComponents();
		this.getRoomInfo();
		this.createMessage();
	}
	/**
	 * Init component of ui.
	 */
	private void initComponents() {
		Class clazz = this.getClass();
		URL background = clazz.getResource("images/BackgroundImage.jpg");
		
		try {
			final Image backgroundImage = javax.imageio.ImageIO.read(background);
			setContentPane(new JPanel(null) {
				@Override 
				public void paintComponent(Graphics g) {
					g.drawImage(backgroundImage, 0, 0, null);
				}
			});
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		URL[] card = new URL[8];
		icon = new ImageIcon[8];
		for (int i = 0; i < icon.length; i++) {
			card[i] = clazz.getResource("images/" + (i + 2) + ".png");
			icon[i] = new ImageIcon(card[i]);
		}

		URL back = clazz.getResource("images/back.png");
		backIcon = new ImageIcon(back);

		Container contents = this.getContentPane();
		contents.setLayout(null);
		JLabel header = new JLabel("Lucky Pick");
		header.setForeground(Color.ORANGE);
		header.setFont(new Font("Tahoma", Font.PLAIN, 34));
		header.setBounds(296, 11, 207, 33);
		contents.add(header);

		chatAreaP = new JTextArea("");
		chatAreaP.setFont(new Font("Monospaced", Font.PLAIN, 16));
		chatAreaP.setLineWrap(true);
		chatAreaP.setEditable(false);
		chatAreaP.setForeground( Color.ORANGE );
		chatAreaP.setOpaque(false);
		chatAreaP.setBackground(new Color(0, 0, 0, 0));
		chatAreaP.setBorder(new SoftBevelBorder(BevelBorder.RAISED) );

		JScrollPane chatPane = new JScrollPane( chatAreaP,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
		chatPane.setSize(254, 303);
		chatPane.setLocation(10, 97);
		chatPane.getViewport().setOpaque(false);
		chatPane.setOpaque(false);
		chatPane.setAutoscrolls(true);
		chatPane.setBackground(new Color(0, 0, 0, 0));
		contents.add(chatPane);

		typeAreaP = new JTextArea("");
		typeAreaP.setFont(new Font("Monospaced", Font.PLAIN, 16));
		typeAreaP.setEditable(true);
		typeAreaP.setBackground( Color.WHITE );
		typeAreaP.setForeground( Color.BLACK );
		typeAreaP.setLineWrap( true );


		JScrollPane typePane = new JScrollPane( typeAreaP,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
		typePane.setSize(254, 120);
		typePane.setLocation(10, 440);
		typePane.setAutoscrolls(true);
		contents.add(typePane);



		JButton sentButton = new JButton("Sent");
		sentButton.setToolTipText("Sent Message");
		sentButton.setBounds(194, 411, 70, 18);
		getContentPane().add(sentButton);

		moveToBottom = new JCheckBox("Bottom");
		moveToBottom.setToolTipText("Scroll to bottom.");
		moveToBottom.setBackground(new Color(0, 0, 0,0));
		moveToBottom.setOpaque(false);
		moveToBottom.setSelected(true);
		moveToBottom.setForeground(Color.ORANGE);
		moveToBottom.setBounds(106, 410, 70, 23);
		contents.add(moveToBottom);

		cardButton = new JButton[8];
		cardListener event = new cardListener();
		for(int i = 0;i < cardButton.length ; i++){
			cardButton[i] = new JButton(backIcon);
			cardButton[i].addActionListener(event);
			contents.add(cardButton[i]);
		}
		cardButton[0].setBounds(296, 97, 100, 140);

		cardButton[1].setBounds(420, 97, 100, 140);

		cardButton[2].setBounds(547, 97, 100, 140);

		cardButton[3].setBounds(673, 97, 100, 140);

		cardButton[4].setBounds(296, 260, 100, 140);

		cardButton[5].setBounds(420, 260, 100, 140);

		cardButton[6].setBounds(547, 260, 100, 140);

		cardButton[7].setBounds(673, 260, 100, 140);

		player2name = new JTextField();
		player2name.setHorizontalAlignment(SwingConstants.CENTER);
		player2name.setEditable(false);
		player2name.setBounds(417, 55, 230, 23);
		contents.add(player2name);
		player2name.setColumns(10);

		player1name = new JTextField();
		player1name.setHorizontalAlignment(SwingConstants.CENTER);
		player1name.setEditable(false);
		player1name.setBounds(417, 433, 230, 23);
		contents.add(player1name);
		player1name.setColumns(10);

		JButton readyButton = new JButton("Start");
		readyButton.setBounds(684, 465, 89, 82);
		contents.add(readyButton);

		JLabel label = new JLabel("RoomID");
		label.setForeground(Color.ORANGE);
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(296, 465, 56, 39);
		contents.add(label);

		JLabel label_1 = new JLabel("Host Name");
		label_1.setForeground(Color.ORANGE);
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setBounds(296, 515, 75, 23);
		contents.add(label_1);

		JLabel label_2 = new JLabel("Game Type");
		label_2.setForeground(Color.ORANGE);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_2.setBounds(478, 467, 75, 33);
		contents.add(label_2);

		JLabel label_3 = new JLabel("Current Player");
		label_3.setForeground(Color.ORANGE);
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_3.setBounds(478, 512, 89, 28);
		contents.add(label_3);

		infoRoomID = new JTextField();
		infoRoomID.setFont(new Font("Tahoma", Font.PLAIN, 17));
		infoRoomID.setEditable(false);
		infoRoomID.setColumns(10);
		infoRoomID.setBounds(374, 467, 94, 33);
		contents.add(infoRoomID);

		infoHostName = new JTextField();
		infoHostName.setFont(new Font("Tahoma", Font.PLAIN, 17));
		infoHostName.setEditable(false);
		infoHostName.setColumns(10);
		infoHostName.setBounds(374, 515, 94, 33);
		contents.add(infoHostName);

		infoGameType = new JTextField();
		infoGameType.setFont(new Font("Tahoma", Font.PLAIN, 17));
		infoGameType.setEditable(false);
		infoGameType.setColumns(10);
		infoGameType.setBounds(577, 467, 94, 33);
		contents.add(infoGameType);

		infoNumplayer = new JTextField();
		infoNumplayer.setFont(new Font("Tahoma", Font.PLAIN, 17));
		infoNumplayer.setEditable(false);
		infoNumplayer.setColumns(10);
		infoNumplayer.setBounds(577, 515, 94, 33);
		contents.add(infoNumplayer);


		SentListener sent = new SentListener();
		sentButton.addActionListener(sent);
		typeAreaP.addKeyListener(new EnterPress());
		readyButton.addActionListener(new StartListener());

		this.setLocation(300,100);
		this.setResizable(false);
		this.setVisible(true);
		this.addWindowListener(new windowListener());
	}
	/**
	 * Create message in an array.
	 */
	public void createMessage(){
		message = new String[51];
		message[0] = "Good to see you.";
		message[1] = "You are a good man.";
		message[2] = "Are you mad?";
		message[3] = "That's it. Click me!";
		message[4] = "Try harder. Go on.";
		message[5] = "Almost done.";
		message[6] = "So boring.";
		message[7] = "The sky is so beatiful, isn't it?";
		message[8] = "Nothing to say.";
		message[9] = "I'm gonna explode.";
		message[10] = "Too much heat.";
		message[11] = "Perfect timing not happen so often.";
		message[12] = "Get everything that you can.";
		message[13] = "Don't waste you time.";
		message[14] = "Life is fun.";
		message[15] = "Keep on what you do.";
		message[16] = "Don't give up.";
		message[17] = "Look at the future.";
		message[18] = "Wanna do something fun?";
		message[19] = "Praise the sun.";
		message[20] = "Obstacles are in everywhere.";
		message[21] = "Good luck.";
		message[22] = "Don't miss a chance.";
		message[23] = "Have you eaten yet?";
		message[24] = "Fill your stomach.";
		message[25] = "Hit the hay.";
		message[26] = "Go away!";
		message[27] = "I'm tired of this shit.";
		message[28] = "Open up your eyes.";
		message[29] = "Be patient.";
		message[30] = "Do you have a cat?";
		message[31] = "Hey! don't click so often.";
		message[32] = "Blank.";
		message[33] = "Try clicking.";
		message[34] = "Go to hell.";
		message[35] = "A card? for what?";
		message[36] = "Don't sit for too long.";
		message[37] = "Let's take a break.";
		message[38] = "What is this smell? Is it from you?";
		message[39] = "Save your money.";
		message[40] = "Dump!";
		message[41] = "Paint the world.";
		message[42] = "Walk straight may be hurt.";
		message[43] = "Does this worth to do.";
		message[44] = "Good life wait ahead.";
		message[45] = "Did you lost your mind.";
		message[46] = "So now, time to say good bye.";
		message[47] = "You should make some money.";
		message[48] = "Whether like it or not, you can't denied it.";
		message[49] = "I see. Your future.";
		message[50] = "Brilliant!";
	}
	/**
	 * Set infomation of the room.
	 * @param roomID is id of the room.
	 * @param hostName is username of the host.
	 * @param gameType is type of the game.
	 * @param numplayer is current number of player.
	 */
	public void setRoomInfo(String roomID, String hostName ,String gameType, String numplayer){
		infoRoomID.setText(roomID);
		infoHostName.setText(hostName);
		infoGameType.setText(gameType);
		infoNumplayer.setText(numplayer);
		player1name.setText(hostName);
		if(numplayer.equals("2")){
			try {
				client.sendToServer("player2name" + client.getUsername());
			} catch (IOException e) {
			}
		}

	}	
	/**
	 * Set current number of player to show.
	 * @param numplayer
	 */
	public void setNumCurrentPlayer(String numplayer){
		infoNumplayer.setText(numplayer);
	}
	/**
	 * Set Name of player to show.
	 * @param name
	 */
	public void setPlayer2Name(String name){
		player2name.setText(name);
	}
	/**
	 * Send a message to server as chat message.
	 */
	public void TypeToServer(){
		try {
			client.sendToServer("sendmessage"+ typeAreaP.getText());
		} catch (IOException e) {}
	}
	/**
	 * Write a message to chat area.
	 * @param message to be written.
	 */
	public void writeToChat(String message){
		chatAreaP.append(message + "\n");
		if(moveToBottom.isSelected() ){
			chatAreaP.setCaretPosition(chatAreaP.getDocument().getLength());
		}
	}
	/**
	 * Require room infomation from server.
	 */
	public void getRoomInfo(){
		try {
			client.sendToServer("getroominfo");
		} catch (IOException e) {
		}
	}
	/**
	 * Show dialog contain message.
	 * @param message to be showed.
	 */
	public void showDialog(String message){
		JOptionPane.showMessageDialog(this,message);
	}
	/**
	 * Window listener to notice when the window is closed.
	 * @author Prin Angkunanuwat
	 *
	 */
	class windowListener implements WindowListener {

		@Override
		public void windowActivated(WindowEvent e) {
		}

		@Override
		public void windowClosed(WindowEvent e) {
		}
		/**
		 * Disconnect from server when close the window.
		 */
		@Override
		public void windowClosing(WindowEvent e) {
			try {
				client.closeConnection();
			} catch (IOException e1) {}
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
		}

		@Override
		public void windowDeiconified(WindowEvent e) {}

		@Override
		public void windowIconified(WindowEvent e) {}

		@Override
		public void windowOpened(WindowEvent e) {}

	}
	/**
	 * Action to send a message to server as chat message.
	 * @author Prin Angkunanuwat
	 *
	 */
	class SentListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed(ActionEvent evt) {
			if ( client.isConnected() ) 
				if( typeAreaP.getText().length() >0 && typeAreaP.getText() != null){
					TypeToServer();
				}
			typeAreaP.setText("");
		}
	}
	/**
	 * Action to send a message to server as chat message.
	 * @author Prin Angkunanuwat
	 *
	 */
	class EnterPress implements KeyListener {
		private boolean controlDown = false;
		private boolean sending = true;
		/** method to perform action when the key is pressed */
		@Override
		public void keyPressed(KeyEvent e) {
			if ( client.isConnected() ) {
				if ( e.getKeyCode() == KeyEvent.VK_CONTROL) {
					controlDown = true;
				} else if ( e.getKeyCode() == KeyEvent.VK_ENTER && !controlDown && sending) {
					sending = false;
					if( typeAreaP.getText().length() >0 && typeAreaP.getText() != null){
						TypeToServer();
					}
					typeAreaP.setText("");
				} else if ( e.getKeyCode() == KeyEvent.VK_ENTER && controlDown) {
					typeAreaP.append("\n");
				} 
			}
		}
		/** method to perform action when the key is released */
		@Override
		public void keyReleased(KeyEvent e) {
			if ( e.getKeyCode() == KeyEvent.VK_CONTROL ){
				controlDown = false;
			} else if ( e.getKeyCode() == KeyEvent.VK_ENTER  && !controlDown ){
				typeAreaP.setText("");
				sending = true;
			}
		}
		@Override
		public void keyTyped(KeyEvent e) {}
	}
	/**
	 * Show useless dialog when the start button is pressed.
	 * @author EstgameTH
	 *
	 */
	class StartListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed(ActionEvent evt) {
			showDialog("Sorry man! this's not for you.");
		}
	}
	/**
	 * Show useless dialog when a card is clicked.
	 * @author EstgameTH
	 *
	 */
	class cardListener implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			int random = (int) Math.floor(Math.random() * 51);
			int randomIcon = (int) Math.floor(Math.random() * 15);
			JButton clickedbutton = (JButton) evt.getSource();
			showDialog(message[random]);
			if(randomIcon <= 7){
				clickedbutton.setIcon(icon[randomIcon]);
			} else {
				clickedbutton.setIcon(backIcon);
			}
		}
	}
}
