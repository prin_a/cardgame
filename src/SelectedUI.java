import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JCheckBox;


/**
 * UI to select room and create room.
 * @author Prin Angkunanuwat
 *
 */
public class SelectedUI extends JFrame{
	private JPanel pane;
	private ChatClientUI mainui;
	private ChatClient client;
	private JTextField infoRoomID;
	private roomSelectListener action;
	private JTextField infoHostName;
	private JTextField infoGameType;
	private JTextField infoNumplayer;
	private ImageIcon iconIcon;
	/**
	 * Construct the ui and set reference.
	 * @param client is main client.
	 * @param mainui is main ui.
	 * @param roomList is list of room from server.
	 */
	public SelectedUI(ChatClient client, ChatClientUI mainui,List<JButton> roomList){
		super("Selection");
		this.client = client;
		this.mainui = mainui;
		this.setSize(800, 600);
		this.initComponents();
		action = new roomSelectListener();
		this.MakeList(roomList);
	}
	/**
	 * Init component of ui.
	 */
	private void initComponents() {
		Class clazz = this.getClass();
		URL background = clazz.getResource("images/BackgroundImage.jpg");
		
		try {
			final Image backgroundImage = javax.imageio.ImageIO.read(background);
			setContentPane(new JPanel(null) {
				@Override 
				public void paintComponent(Graphics g) {
					g.drawImage(backgroundImage, 0, 0, null);
				}
			});
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		URL enter = clazz.getResource("images/enter.png");
		ImageIcon enterIcon = new ImageIcon(enter);


		URL host = clazz.getResource("images/host.png");
		ImageIcon hostIcon = new ImageIcon(host);

		URL icon = clazz.getResource("images/Icon.png");
		iconIcon = new ImageIcon(icon);

		Container contents = this.getContentPane();
		contents.setLayout(null);
		JLabel header = new JLabel("Select Room");
		header.setForeground(Color.ORANGE);
		header.setFont(new Font("Tahoma", Font.PLAIN, 34));
		header.setBounds(259, 11, 207, 33);
		contents.add(header);

		pane = new JPanel();
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

		JScrollPane scrollPane = new JScrollPane(pane, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
		scrollPane.setSize(207, 505);
		scrollPane.setLocation(35, 55);
		contents.add(scrollPane);


		JPanel panel = new JPanel();
		panel.setBackground(new Color(0,0,0,0));
		panel.setOpaque(false);
		panel.setForeground(Color.ORANGE);
		panel.setBounds(269, 55, 490, 350);
		contents.add(panel);
		panel.setLayout(null);


		JLabel lblRoomid = new JLabel("RoomID");
		lblRoomid.setForeground(Color.ORANGE);
		lblRoomid.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblRoomid.setBounds(10, 11, 78, 39);
		panel.add(lblRoomid);

		infoRoomID = new JTextField();
		infoRoomID.setEditable(false);
		infoRoomID.setFont(new Font("Tahoma", Font.PLAIN, 17));
		infoRoomID.setBounds(154, 11, 139, 39);
		panel.add(infoRoomID);
		infoRoomID.setColumns(10);

		JLabel lblNewLabel = new JLabel("Host Name");
		lblNewLabel.setForeground(Color.ORANGE);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNewLabel.setBounds(10, 86, 111, 39);
		panel.add(lblNewLabel);

		infoHostName = new JTextField();
		infoHostName.setEditable(false);
		infoHostName.setFont(new Font("Tahoma", Font.PLAIN, 17));
		infoHostName.setBounds(154, 86, 139, 39);
		panel.add(infoHostName);
		infoHostName.setColumns(10);

		JLabel lblGameType = new JLabel("Game Type");
		lblGameType.setForeground(Color.ORANGE);
		lblGameType.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblGameType.setBounds(10, 150, 111, 39);
		panel.add(lblGameType);

		infoGameType = new JTextField();
		infoGameType.setEditable(false);
		infoGameType.setFont(new Font("Tahoma", Font.PLAIN, 17));
		infoGameType.setBounds(154, 150, 139, 39);
		panel.add(infoGameType);
		infoGameType.setColumns(10);

		JLabel lblNumberOfPlayer = new JLabel("Current Player");
		lblNumberOfPlayer.setForeground(Color.ORANGE);
		lblNumberOfPlayer.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblNumberOfPlayer.setBounds(10, 226, 139, 39);
		panel.add(lblNumberOfPlayer);

		infoNumplayer = new JTextField();
		infoNumplayer.setEditable(false);
		infoNumplayer.setFont(new Font("Tahoma", Font.PLAIN, 17));
		infoNumplayer.setBounds(154, 226, 139, 39);
		panel.add(infoNumplayer);
		infoNumplayer.setColumns(10);

		JButton btnConnect = new JButton(enterIcon);
		btnConnect.setToolTipText("Enter Room");
		btnConnect.setBounds(526, 433, 233, 127);
		contents.add(btnConnect);

		JButton btnCreate = new JButton(hostIcon);
		btnCreate.setToolTipText("Create Room");
		btnCreate.setBounds(269, 433, 233, 127);
		contents.add(btnCreate);

		JLabel lblRoomList = new JLabel("Room List");
		lblRoomList.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblRoomList.setForeground(Color.ORANGE);
		lblRoomList.setBounds(35, 30, 63, 14);
		contents.add(lblRoomList);

		btnConnect.addActionListener(new playListener());	
		btnCreate.addActionListener(new createListener());

		this.setVisible(true);
		this.setLocation(400,100);
		this.setResizable(false);
	}
	/**
	 * Require room list from server.
	 */
	public void getRoomList(){
		try {
			client.sendToServer("reroomlist");
		} catch (IOException e) {
			showDialog("failed to get room.");
		}
	}
	/**
	 * Show dialog contain message.
	 * @param message to be showed.
	 */
	public void showDialog(String message){
		JOptionPane.showMessageDialog(this,message);
	}
	/**
	 * Make list of rooms as a list of buttons.
	 * @param roomList is list of room.
	 */
	public void MakeList(List<JButton> roomList){
		for(JButton b: roomList){
			pane.add(b);
			b.addActionListener(action);
		}
	}
	/**
	 * Popup dialog with a choice to be selected as game type.
	 * @return
	 */
	public String selectGameType(){
		Object[] listGameType = {"LuckyPick"};
		String s = (String)JOptionPane.showInputDialog(
				this,
				"Select game type:\n",
				"Choose game type",
				JOptionPane.PLAIN_MESSAGE,
				iconIcon,
				listGameType,
				"LuckyPick");
		return s;
	}
	/**
	 * Set room info to show up.
	 * @param roomID is id of room.
	 * @param hostName is username of the host.
	 * @param gameType is type of the game in room.
	 * @param numplayer is number of current player in the room.
	 */
	public void setRoomInfo(String roomID, String hostName ,String gameType, String numplayer){
		infoRoomID.setText(roomID);
		infoHostName.setText(hostName);
		infoGameType.setText(gameType);
		infoNumplayer.setText(numplayer);
	}
	/**
	 * Action when a play button is clicked.
	 * @author Prin Angkunanuwat
	 *
	 */
	class playListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed(ActionEvent evt) {
			if(infoRoomID.getText() != ""){
				try {
					client.sendToServer("joinroomnum " + infoRoomID.getText() );
				} catch (IOException e) {
				}
			}
		}
	}
	/**
	 * Action when a create button is clicked.
	 * @author Prin Angkunanuwat
	 *
	 */
	class createListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed(ActionEvent evt) {
			String type = selectGameType();
			if(type != null){
				try {
					client.sendToServer("createroom " + type);
				} catch (IOException e) {
				}
			}


		}
	}
	/**
	 * Action when a room button in the list is clicked.
	 * @author Prin Angkunanuwat
	 *
	 */
	class roomSelectListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed(ActionEvent evt) {
			try {
				client.sendToServer("getroominfo" + ((JButton) evt.getSource()).getName());
			} catch (IOException e) {
			}

		}
	}
}