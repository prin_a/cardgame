import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;

import java.awt.Button;

/**
 * UI for chat client or main client.
 * @author Prin Angkunanuwat
 *
 */
public class ChatClientUI extends JFrame{
	private JTextArea chatArea;
	private JTextArea typeArea;
	private JTextArea listArea;
	private JComboBox<String> listBox;
	private JCheckBox moveToBottom;
	private JCheckBox alertChat;
	private JTextField username;
	private List<JButton> roomList;
	private SelectedUI subui;

	private ChatClient client;
	private List<String> listName;
	/**
	 * Construct ui with client.
	 * @param client to be refered.
	 */
	public ChatClientUI(ChatClient client){
		super("Chat");
		this.client = client;
		this.client.setUI(this);
		this.setSize(800, 600);
		roomList = new ArrayList<JButton>();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.initComponents();
	}
	/**
	 * Init component of ui.
	 */
	private void initComponents() {
		Class clazz = this.getClass();
		URL background = clazz.getResource("images/BackgroundImage.jpg");
		
		try {
			final Image backgroundImage = javax.imageio.ImageIO.read(background);
			setContentPane(new JPanel(null) {
				@Override 
				public void paintComponent(Graphics g) {
					g.drawImage(backgroundImage, 0, 0, null);
				}
			});
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		URL dice = clazz.getResource("images/Dice.png");
		ImageIcon diceIcon = new ImageIcon(dice);

		URL play = clazz.getResource("images/play.png");
		ImageIcon playIcon = new ImageIcon(play);

		Container contents = this.getContentPane();
		contents.setLayout(null);
		JLabel header = new JLabel("Lobby Room");
		header.setForeground(Color.ORANGE);
		header.setFont(new Font("Tahoma", Font.PLAIN, 34));
		header.setBounds(166, 11, 207, 33);
		contents.add(header);

		JLabel inputName = new JLabel("Your name");
		inputName.setForeground(Color.ORANGE);
		inputName.setBounds(483, 21, 74, 23);
		contents.add(inputName);

		username = new JTextField(10);
		username.setBounds(567, 21, 114, 23);
		contents.add(username);

		JButton connectButton = new JButton("Connect");
		connectButton.setToolTipText("Start connection");
		connectButton.setBounds(695, 21, 89, 23);
		contents.add(connectButton);

		listArea = new JTextArea("OnlineList\n");
		listArea.setFont(new Font("Monospaced", Font.PLAIN, 16));
		listArea.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		listArea.setLineWrap(true);
		listArea.setEditable(false);
		listArea.setForeground( Color.ORANGE );
		listArea.setOpaque(false);
		listArea.setBackground(new Color(0, 0, 0, 0));
		listArea.setBorder(new SoftBevelBorder(BevelBorder.RAISED) );

		JScrollPane listuser = new JScrollPane( listArea,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
		listuser.setSize(217, 350);
		listuser.setLocation(567, 50);
		listuser.getViewport().setOpaque(false);
		listuser.setOpaque(false);
		listuser.setAutoscrolls(true);
		listuser.setBackground(new Color(0, 0, 0, 0));
		contents.add(listuser);

		chatArea = new JTextArea("");
		chatArea.setFont(new Font("Monospaced", Font.PLAIN, 16));
		chatArea.setLineWrap(true);
		chatArea.setEditable(false);
		chatArea.setForeground( Color.ORANGE );
		chatArea.setOpaque(false);
		chatArea.setBackground(new Color(0, 0, 0, 0));
		chatArea.setBorder(new SoftBevelBorder(BevelBorder.RAISED) );

		JScrollPane chatPane = new JScrollPane( chatArea,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
		chatPane.setSize(550, 350);
		chatPane.setLocation(10, 50);
		chatPane.getViewport().setOpaque(false);
		chatPane.setOpaque(false);
		chatPane.setAutoscrolls(true);
		chatPane.setBackground(new Color(0, 0, 0, 0));
		contents.add(chatPane);

		typeArea = new JTextArea("");
		typeArea.setFont(new Font("Monospaced", Font.PLAIN, 16));
		typeArea.setEditable(true);
		typeArea.setBackground( Color.WHITE );
		typeArea.setForeground( Color.BLACK );
		typeArea.setLineWrap( true );


		JScrollPane typePane = new JScrollPane( typeArea,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
		typePane.setSize(550, 120);
		typePane.setLocation(10, 440);
		typePane.setAutoscrolls(true);
		contents.add(typePane);


		JLabel toLabel = new JLabel("Sent To :");
		toLabel.setForeground(Color.ORANGE);
		toLabel.setBounds(10, 411, 87, 18);
		contents.add(toLabel);


		listName = new ArrayList<String>();
		listBox = new JComboBox<String>();
		updateList();
		listBox.setBounds(67, 404, 114, 33);
		contents.add(listBox);

		JButton sentButton = new JButton("Sent");
		sentButton.setToolTipText("Sent Message");
		sentButton.setBounds(194, 411, 70, 18);
		getContentPane().add(sentButton);

		moveToBottom = new JCheckBox("Bottom");
		moveToBottom.setToolTipText("Scroll to bottom.");
		moveToBottom.setBackground(new Color(0, 0, 0,0));
		moveToBottom.setOpaque(false);
		moveToBottom.setSelected(true);
		moveToBottom.setForeground(Color.ORANGE);
		moveToBottom.setBounds(270, 407, 70, 23);
		contents.add(moveToBottom);

		alertChat = new JCheckBox("Alert");
		alertChat.setToolTipText("Alert chat update.");
		alertChat.setBackground(new Color(0, 0, 0,0));
		alertChat.setOpaque(false);
		alertChat.setSelected(true);
		alertChat.setForeground(Color.ORANGE);
		alertChat.setBounds(350, 407, 55, 23);
		contents.add(alertChat);

		JButton RollDiceButton = new JButton(diceIcon);
		RollDiceButton.setToolTipText("Roll the dice");
		RollDiceButton.setBounds(410, 409, 20, 20);
		contents.add(RollDiceButton);

		JButton playButton = new JButton(playIcon);
		playButton.setToolTipText("Select Room");
		playButton.setBounds(567, 440, 217, 120);
		contents.add(playButton);


		SentListener sent = new SentListener();
		sentButton.addActionListener(sent);
		typeArea.addKeyListener(new EnterPress());
		ConnectListener connectL = new ConnectListener();
		connectButton.addActionListener(connectL);
		username.addActionListener(connectL);
		RollDiceButton.addActionListener(new DiceListener());
		playButton.addActionListener(new playListener());

		this.setLocation(300,100);
		this.setResizable(false);
		this.setVisible(true);
		this.addWindowListener(new windowListener());
	}
	/**
	 * Send a message to server as chat message.
	 */
	public void TypeToServer(){
		try {
			String toWhom = (String) listBox.getSelectedItem();
			client.sendToServer("Send m To: " + toWhom + " " + typeArea.getText());
		} catch (IOException e) {}
	}
	/**
	 * Change editable of username text.
	 * @param b is boolean.
	 */
	public void setEditUsername(boolean b){
		username.setEditable(b);
	}
	/**
	 * Write a message to chat area.
	 * @param message to be written.
	 */
	public void writeToChat(String message){
		chatArea.append(message + "\n");
		if(moveToBottom.isSelected() ){
			chatArea.setCaretPosition(chatArea.getDocument().getLength());
		}
		if(alertChat.isSelected()){
			java.awt.Toolkit.getDefaultToolkit().beep();
		}
	}
	/**
	 * Get username of user.
	 * @return username of user.
	 */
	public String getUsername(){
		return username.getText();
	}
	/**
	 * Add name to the list.
	 * @param name to be added.
	 */
	public void addToListName(String name){
		listName.add(name);
	}
	/**
	 * Get all room in the list.
	 * @return roomlist.
	 */
	public List<JButton> getRoomList(){
		return roomList;
	}
	/**
	 * Clear list of name.
	 */
	public void clearListName(){
		listName.clear();
	}
	/**
	 * Update list of online user.
	 */
	public void updateList(){
		listBox.removeAllItems();
		listBox.addItem("All");
		listArea.setText("OnlineList\n");
		for(String s: listName){
			listBox.addItem(s);
			listArea.append(s+"\n");
		}
	}
	/**
	 * Show dialog contain message.
	 * @param message to be showed.
	 */
	public void showDialog(String message){
		JOptionPane.showMessageDialog(this,message);
	}
	/**
	 * Create room selection ui.
	 */
	public void createSubui(){
		subui = new SelectedUI(this.client, this,roomList);
	}
	/**
	 * Get the room selection ui.
	 * @return room selection ui.
	 */
	public SelectedUI getSubUI(){
		return subui;
	}
	/**
	 * Add room to the room list.
	 * @param roomID to be add.
	 */
	public void addRoom(String roomID){
		roomList.add(new JButton(roomID));
		roomList.get(roomList.size() - 1).setName(roomID);;
	}
	/**
	 * Clear the room list.
	 */
	public void clearRoom(){
		roomList.clear();
	}
	/**
	 * Window listener to notice when the window is closed.
	 * @author Prin Angkunanuwat
	 *
	 */
	class windowListener implements WindowListener {

		@Override
		public void windowActivated(WindowEvent e) {
		}
		/**
		 * Disconnect from server when close the window.
		 */
		@Override
		public void windowClosed(WindowEvent e) {
			try {
				client.closeConnection();
			} catch (IOException e1) {}
		}

		@Override
		public void windowClosing(WindowEvent e) {}

		@Override
		public void windowDeactivated(WindowEvent e) {
		}

		@Override
		public void windowDeiconified(WindowEvent e) {}

		@Override
		public void windowIconified(WindowEvent e) {}

		@Override
		public void windowOpened(WindowEvent e) {}

	}
	/**
	 * Start connection with server.
	 * @author Prin Angkunanuwat
	 *
	 */
	class ConnectListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed(ActionEvent evt) {

			try {
				if(!client.isConnected()){
					String name = username.getText();
					if( name != null && name.length() > 0 && name.length() < 11 && name.matches("\\w+") && !name.equals("All")) {
						client.openConnection();
						client.sendToServer("connect as " + name);
						setEditUsername(false);
						writeToChat("Connect as " + name + ".");
					} else {
						showDialog("Name must be less than 11 charactor and has no special charactor.");
					}
				}
			} catch (IOException e) {
				showDialog("Can't connect to server.");
			}

		}
	}
	/**
	 * Action to send a message to server as chat message.
	 * @author Prin Angkunanuwat
	 *
	 */
	class SentListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed(ActionEvent evt) {
			if ( client.isConnected() ) 
				if( typeArea.getText().length() >0 && typeArea.getText() != null){
					TypeToServer();
				}
			typeArea.setText("");
		}
	}
	/**
	 * Action to send a message to server as chat message.
	 * @author Prin Angkunanuwat
	 *
	 */
	class EnterPress implements KeyListener {
		private boolean controlDown = false;
		private boolean sending = true;
		@Override
		/** method to perform action when the key is pressed */
		public void keyPressed(KeyEvent e) {
			if ( client.isConnected() ) {
				if ( e.getKeyCode() == KeyEvent.VK_CONTROL) {
					controlDown = true;
				} else if ( e.getKeyCode() == KeyEvent.VK_ENTER && !controlDown && sending) {
					sending = false;
					if( typeArea.getText().length() >0 && typeArea.getText() != null){
						TypeToServer();
					}
					typeArea.setText("");
				} else if ( e.getKeyCode() == KeyEvent.VK_ENTER && controlDown) {
					typeArea.append("\n");
				} 
			}
		}
		/** method to perform action when the key is released */
		@Override
		public void keyReleased(KeyEvent e) {
			if ( e.getKeyCode() == KeyEvent.VK_CONTROL ){
				controlDown = false;
			} else if ( e.getKeyCode() == KeyEvent.VK_ENTER  && !controlDown ){
				typeArea.setText("");
				sending = true;
			}
		}
		@Override
		public void keyTyped(KeyEvent e) {}
	}
	/**
	 * Roll the dice and get random number between 0 - 100.
	 * @author Prin Angkunanuwat
	 *
	 */
	class DiceListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed(ActionEvent evt) {
			if ( client.isConnected() ) {
				try {
					client.sendToServer("rollTheDice");
					showDialog("Roll the dice!!!!");
				} catch (IOException e) {
					showDialog("Fail to roll the dice.");
				}
			}

		}
	}
	/**
	 * Open the room selection ui. 
	 * @author Prin Angkunanuwat.
	 *
	 */
	class playListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed(ActionEvent evt) {
			if ( client.isConnected() ) {
				try {
					client.sendToServer("reroomlist ");
					createSubui();
				} catch (IOException e) {
					showDialog("failed to get room.");
				}
			} else showDialog("Connect to server first.");

		}
	}
}
