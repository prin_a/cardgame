
/**
 * Main class to run the program.
 * @author Prin Angkunanuwat
 *
 */
public class MysteriousCard {
	/**
	 * Main method to run the program.
	 * @param args is not used.
	 */
	public static void main(String[] args){
		ChatClient client = new ChatClient("171.98.209.127",5001);
		ChatClientUI ui = new ChatClientUI(client);
	}
}
