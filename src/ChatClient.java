import java.io.IOException;

import com.lloseng.ocsf.client.AbstractClient;

/**
 * Main client for card game.
 * @author Prin Angkunanuwat
 *
 */
public class ChatClient extends AbstractClient {
	private ChatClientUI ui;
	private GameClient game;
	private LuckyPickClientUI gameui;
	/**
	 * Constructure of client.
	 * @param host name of server.
	 * @param port used in server.
	 */
	public ChatClient(String host, int port) {
		super(host, port);
	}
	/**
	 * Recieve message from server and decide what to do.
	 * Take first eleven charractor as a header.
	 */
	protected void handleMessageFromServer(Object msg) {
		String message = (String) msg;
		String header = message.substring(0, 11);
		
		switch(header){
		case "s send msg ":
			ui.writeToChat(message.substring(11).trim());
			break;
		case "listClient ":
			String[] list = message.substring(11).trim().split(" ");
			ui.clearListName();
			for(String s: list){
				ui.addToListName(s);
			}
			ui.updateList();
			break;
		case "listschange":
			try {
				sendToServer("requestlist");
			} catch (IOException e) {}
			break;
		case "listrooms  ":
			String[] listroom = message.substring(11).trim().split(" ");
			ui.clearRoom();
			for(String s: listroom){
				ui.addRoom(s);
			}
			break;
		case "retroominfo":
			String[] roomInfo = message.substring(11).trim().split(" ");
			String roomID = roomInfo[0];
			String hostName = roomInfo[1];
			String gameType = roomInfo[2];
			String numplayer = roomInfo[3];
			ui.getSubUI().setRoomInfo(roomID, hostName, gameType, numplayer);
			break;
		case "createcompt":
			String[] splitz = message.substring(11).trim().split(" ", 2);
			if(splitz[0].equalsIgnoreCase("LuckyPick")){
				try {
					game = new LuckyPickClient("171.98.209.127",Integer.parseInt(splitz[1]));
					game.setUsername(ui.getUsername());
					game.openConnection();
					gameui = new LuckyPickClientUI((LuckyPickClient) game );
					ui.getSubUI().dispose();
					
				} catch (IOException e) {
				}
			}
			break;
		case "enterroomid":
			try {
				game = new LuckyPickClient("171.98.209.127",Integer.parseInt(message.substring(15).trim()));
				game.setUsername(ui.getUsername());
				game.openConnection();
				gameui = new LuckyPickClientUI((LuckyPickClient) game );
				ui.getSubUI().dispose();
			} catch (IOException e) {
				ui.getSubUI().showDialog("Failed to join game.");
			}
			break;
		case "dialogsubui":
			ui.getSubUI().showDialog(message.substring(11).trim());
			break;
			
		}
	}
	/**
	 * Perform an action when lost connection to server.
	 */
	protected void connectionClosed(){
		ui.writeToChat("Disconnected");
		ui.setEditUsername(true);
	}
	/**
	 * Set ui to be used.
	 * @param ui to be refered.
	 */
	public void setUI(ChatClientUI ui){
		this.ui = ui;
	}
	
}
