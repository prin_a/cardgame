import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;

/**
 * Main server of the program.
 * @author Prin Angkunanuwat
 *
 */
public class ChatServer extends AbstractServer{

	private List<ConnectionToClient> clientList;
	private List<GameServer> serverList;
	private int roomCount = 5002;
	/**
	 * Construct server with port.
	 * @param port used by server.
	 */
	public ChatServer(int port) {
		super(port);
		clientList = new ArrayList<ConnectionToClient>();
		serverList = new ArrayList<GameServer>();
	}
	/**
	 * Recieve message from client and decide what to do.
	 * Take first eleven charractor as a header.
	 */
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		if ( ! ( msg instanceof String )) {
			sendToClient(client,"s send msg Unrecognized Message");
			return;
		}

		String message = (String) msg;
		String header = message.substring(0, 11);
		String cutString = "";
		
		switch(header){
		case "connect as ":
			String username = message.substring(11).trim();
			client.setInfo("username", username);
			sendToAllClients( "s send msg " + client.getInfo("username") + " has joined the chat.");
			sendToAllClients("listschange");
			break;
		case "Send m To: ":
			cutString = message.substring(11).trim();
			String[] splitz = cutString.split(" ", 2);
			if(splitz[0].equalsIgnoreCase("All")){
				sendToAllClients( "s send msg " + client.getInfo("username") + ": " + splitz[1].trim());
			} else {
				boolean success = false;
				for(ConnectionToClient c : clientList){
					if( c.getInfo("username").equals(splitz[0])){
						sendToClient(c, "s send msg " + "Whisper from " + client.getInfo("username") + ": " + splitz[1].trim());
						success = true;
					}
				}
				if(success){
					sendToClient(client,"s send msg " + "To " + splitz[0] + ": " + splitz[1].trim());
				} else {
					sendToClient(client,"s send msg " + "Fail to send message to " + splitz[0]);
				}
			}
			break;
		case "requestlist":
			String listOfClient = "listClient ";
			for( ConnectionToClient c : clientList){
				listOfClient += c.getInfo("username") + " ";
			}
			sendToClient(client,listOfClient);
			break;
		case "rollTheDice":
			int ranNumber = 0;
			for(int i = 0; i< 10 ; i++){
				ranNumber += (int) Math.floor(Math.random() * 11);
			}
			sendToAllClients("s send msg " + client.getInfo("username") + " roll " + ranNumber + " out of 100.");
			break;
		case "createroom ":
			String type = message.substring(11).trim();
			if( type.equalsIgnoreCase("LuckyPick")){
				try {
					GameServer luckysv = new LuckyPickServer(roomCount, roomCount , (String) client.getInfo("username"),this);
					luckysv.listen();
					serverList.add(luckysv);
					sendToClient(client,"createcompt LuckyPick " + roomCount++ );
				} catch (IOException e) {
					sendToClient(client, "dialogsubui  Create failed.");
				}
				
			} 
			break;
		case "reroomlist ":
			String listOfRoom = "listrooms  ";
			for( GameServer s : serverList){
				listOfRoom +=  s.getRoomID() + " ";
			}
			sendToClient(client,listOfRoom);
			break;
		case "getroominfo":
			String wantedroom = message.substring(11).trim();
			int index = -1;
			for( int i = 0; i<serverList.size() ;i++ ){
				if( serverList.get(i).getRoomID().equalsIgnoreCase(wantedroom) ){
					index = i;
					break;
				}
			}
			if(index == -1 ) break;
			String roomInfo = "retroominfo" + serverList.get(index).getRoomInfo();
			sendToClient(client,roomInfo);
			break;
		case "joinroomnum":
			String joinedroom = message.substring(11).trim();
			int dex = -1;
			for( int i = 0; i<serverList.size() ;i++ ){
				if( serverList.get(i).getRoomID().equalsIgnoreCase(joinedroom) ){
					dex = i;
					break;
				}
			}
			if(dex == -1 ) break;
			if(serverList.get(dex).getNumplayer() < 2){
				sendToClient(client,"enterroomid" +  serverList.get(dex).getRoomID());
			} else {
				sendToClient(client,"dialogsubui" + "full!");
			}
			break;
			
		}
	}
	/**
	 * Add client to the list when client connect.
	 */
	protected void clientConnected(ConnectionToClient client) {
		clientList.add(client);
		System.out.println(client + " connect.");
	}
	/**
	 * Remove client from the list and send message to all client when client disconnect.
	 */
	synchronized protected void clientDisconnected(ConnectionToClient client) {
		clientList.remove(client);
		sendToAllClients("s send msg " + client.getInfo("username") + " has left the chat.");
		sendToAllClients("listschange");
	}
	/**
	 * Send message to specific client.
	 * @param client to receive message.
	 * @param message to be send.
	 */
	protected void sendToClient(ConnectionToClient client, String message){
		try {
			client.sendToClient(message);
		} catch (IOException e) {
		}
	}
	/**
	 * Remove game server from the list.
	 * @param server to be removed.
	 */
	public void removeServer(GameServer server){
		serverList.remove(server);
	}

}
