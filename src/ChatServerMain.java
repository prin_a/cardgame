import java.io.IOException;

/**
 * Main class to start the server.
 * @author Prin Angkunanuwat
 *
 */
public class ChatServerMain {
	private static final int PORT = 5001;
	/**
	 * Main method to start server.
	 * @param args is not used.
	 */
	public static void main(String[] args){
		ChatServer server = new ChatServer(PORT);
		try {
			server.listen();
			System.out.println("Listening on port 5001" );
			
		} catch (IOException e) {
			System.out.println("Couldn't start server:");
			System.out.println(e);
		}
	}
}
