/**
 * Inter face for game client.
 * @author Prin Angkunanuwat
 *
 */
public interface GameClient {
	void openConnection() throws java.io.IOException;
	/**
	 * Set username of client.
	 * @param name is name of user who use the client.
	 */
	void setUsername(String name);
}
