

import java.io.IOException;

import com.lloseng.ocsf.client.AbstractClient;
/**
 * Client for luckypick game.
 * @author Prin Angkunanuwat
 *
 */
public class LuckyPickClient extends AbstractClient implements GameClient{

	private LuckyPickClientUI ui;
	private String username;
	/**
	 * Construct client with given port and host name.
	 * @param host is host name of server.
	 * @param port is port used for server.
	 */
	public LuckyPickClient(String host, int port) {
		super(host, port);
	}
	/**
	 * Receive message from server and decide what to do.
	 * Take first eleven charractor as a header.
	 */
	protected void handleMessageFromServer(Object msg) {
		String message = (String) msg;
		String header = message.substring(0, 11);

		switch(header){
		case "sendmessage":
			ui.writeToChat(message.substring(11).trim());
			break;
		case "retroominfo":
			String[] roomInfo = message.substring(11).trim().split(" ");
			String roomID = roomInfo[0];
			String hostName = roomInfo[1];
			String gameType = roomInfo[2];
			String numplayer = roomInfo[3];
			ui.setRoomInfo(roomID, hostName, gameType, numplayer);
			break;
		case "numchanges ":
			String newNum = message.substring(11).trim();
			ui.setNumCurrentPlayer(newNum);
			if( newNum.equals("1")){
				ui.setPlayer2Name("");
			}
			break;
		case "requestname":
			
			break;
		case "player2name":
			ui.setPlayer2Name(message.substring(11).trim());
			break;

		}
	}
	/**
	 * Write disconnect message when lost connection.
	 */
	protected void connectionClosed(){
		ui.writeToChat("Disconnected");
	}
	/**
	 * Set reference ui.
	 * @param ui to be set.
	 */
	public void setUI(LuckyPickClientUI ui){
		this.ui = ui;
	}
	/**
	 * Set username of user use this client.
	 */
	public void setUsername(String name){
		username = name;
	}
	/**
	 * Get username from client.
	 * @return username of user that use client.
	 */
	public String getUsername(){
		return username;
	}

}
