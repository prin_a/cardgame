/**
 * Interface for game server.
 * @author Prin Angkunanuwat
 *
 */
public interface GameServer {
	void listen() throws java.io.IOException;
	/**
	 * Get room id of server.
	 * @return room id of server.
	 */
	String getRoomID();
	/**
	 * Get current number of player in the room.
	 * @return number of player.
	 */
	int getNumplayer();
	/**
	 * Get all information of the room.
	 * @return information of the room.
	 */
	String getRoomInfo();
}
